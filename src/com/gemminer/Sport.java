/**
 * Sport Device control Java interface.
 */

package com.gemminer;

import java.util.concurrent.TimeoutException;

public class Sport {
    static {
        System.loadLibrary("sport_jni");
    };

    /**
     * Constant for Set Download_Pin to Low Level.
     */
    public static final int DOWNLOAD_PIN_LOW  = 0;

    /**
     * Constant for Set Download_Pin to High Level.
     */
    public static final int DOWNLOAD_PIN_HIGH = 1;

    /**
     * Set Download Pin Logic.
     *
     * @param isHigh the Download_Pin define.
     */
    public static native void downloadPin(int isHigh);

    /**
     * Send data to Uart.
     *
     * @param data for Uart TX.
     * @param len the data length.
     * @return Return data sent. if error it should be -1.
     */
    public static native int sendBytes(byte[] data, int len);
    
    /**
     * Get 6 bytes data from Uart.
     *
     * @return 6 bytes data from UART.
     */
    public static native byte[] getBytes();
    
    /**
     * Get N Bytes data from Uart.
     *
     * @param len for receive data size.
     * @return len bytes data from UART.
     */
    public static native byte[] getNBytes(int len);

    /**
     * Catch N Bytes data from Uart, with timeout 300ms.
     *
     * @param len for receive data size.
     * @return len bytes data from UART.
     */
    public static native byte[] catchNBytes(int len) throws TimeoutException;

    /**
     * Peek N Bytes data from Uart.
     *
     * @param len for receive data size.
     * @return len bytes data from UART.
     */
    public static native byte[] dumpNBytes(int len);
    
    
    /**
     * Set Uart baudrate.
     *
     * @param speed baudrate to Uart.
     */
    public static native void setSpeed(int speed);

    /**
     * Set parity
     *
     * @param parity 0x45 is 'E', others is 'N'
     */
    public static native void setParity(byte parity);

    /**
     * Setup log level
     *
     * @param level can setup log level with 0(close) to 5(all).
     */
    public static native void log(int level);

    /**
     * Init JNI Uart module.
     */
    public static native void init();

    /**
     * Deinit JNI Uart module.
     */
    public static native void deinit();
}

