package endexcase.scanmachine.uart;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.concurrent.TimeoutException;

import endexcase.scanmachine.util.AppLog;

import android.os.SystemClock;

public class STM32 {
	private static final AppLog smAppLog = new AppLog("uart", STM32.class);
	/* device table */
	/**
		uint16_t	id;
		char		*name;
		uint32_t	ram_start, ram_end;
		uint32_t	fl_start, fl_end;
		uint16_t	fl_pps; // pages per sector
		uint16_t	fl_ps;  // page size
		uint32_t	opt_start, opt_end;
		uint32_t	mem_start, mem_end;
	*/
//	int stm32_dev_t devices[] = {
//		{0x412, "Low-density"      , 0x20000200, 0x20002800, 0x08000000, 0x08008000, 4, 1024, 0x1FFFF800, 0x1FFFF80F, 0x1FFFF000, 0x1FFFF800},
//		{0x410, "Medium-density"   , 0x20000200, 0x20005000, 0x08000000, 0x08020000, 4, 1024, 0x1FFFF800, 0x1FFFF80F, 0x1FFFF000, 0x1FFFF800},
//		{0x414, "High-density"     , 0x20000200, 0x20010000, 0x08000000, 0x08080000, 2, 2048, 0x1FFFF800, 0x1FFFF80F, 0x1FFFF000, 0x1FFFF800},
//		{0x418, "Connectivity line", 0x20001000, 0x20010000, 0x08000000, 0x08040000, 2, 2048, 0x1FFFF800, 0x1FFFF80F, 0x1FFFB000, 0x1FFFF800},
//		{0x420, "Medium-density VL", 0x20000200, 0x20002000, 0x08000000, 0x08020000, 4, 1024, 0x1FFFF800, 0x1FFFF80F, 0x1FFFF000, 0x1FFFF800},
//		{0x430, "XL-density"       , 0x20000800, 0x20018000, 0x08000000, 0x08100000, 2, 2048, 0x1FFFF800, 0x1FFFF80F, 0x1FFFE000, 0x1FFFF800},
//		{0x0}
//	};
//	public static final String FrimwareRecoverPath ="/system/frimware/";
//	public static final String FrimwareRecoverfileName = "ENDEXDOWNLOAD.bin";	
	public static final String FrimwareRecoverPath ="/storage/sdcard0/Download/";
	public static final String FrimwareRecoverfileName = "ENDEXDOWNLOAD.bin";
	static byte[] flashStartMemoryAddress = {0x08,0x00,0x00,0x00};
	static byte[] TempAddress={0x08,0x00,0x00,0x00};
	public static final byte[] ACK  = {(byte) 0x79 };
	public static final byte[] NACK = {(byte) 0x1F };
	public static final byte[] INIT = {(byte) 0x7F };
	public static final byte[] UNLOCK = {(byte) 0x92,(byte) 0x6D };
	public static final byte[] PROTECT = {(byte) 0x82,(byte) 0x7D };
	public static final byte[] GET  = { 0x00,(byte)0xFF};
	public static final byte[] ERASE= { 0x43,(byte)0xBC };
	public static final byte[] ERASE_GLOBAL= {(byte) 0xFF,0x00 };
	public static final byte[] WRITE_MEMORY= {(byte) 0x31,(byte) 0xCE };
	public static final byte[] GO= {(byte) 0x21,(byte) 0xDE };
	static String result ="";
	
	// address CheckSum
	public static byte CheckSum(byte[] v,int length){
		byte after = 0x00;
		for(int i=0;i<length;i++){
		after ^= v[i];
		}
		return after;
	}
	
	// data Stream CheckSum
	public static byte CheckSum(byte len,byte[] v,int length){
		for(int i=0;i<length;i++){
			len ^= v[i];
		} 
		return len;
	}
	
	public static boolean Nack(){
		byte[] result=NACK;
		try {
			result = SportInterface.catchNBytes(1);
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		
		if(result==NACK){
			return true;
		}
		else{
			return false;
		}
	}
	
	static void AbsoluteSleep(int a){
		long LeaveTime = System.currentTimeMillis()+a;
		do{
			if(System.currentTimeMillis()>=LeaveTime)
				break;
		}while(true);
	}
	
	static String ByteArraryToHexString(byte[] ByteArray) {
		char[] HexArray = {'0','1','2','3','4','5','6','7',
				'8','9','A','B','C','D','E','F'};
		char[] HexChars = new char[ByteArray.length * 2];
		int value;
		for ( int j = 0; j < ByteArray.length; j++ ) {
			value = ByteArray[j] & 0xFF;
			HexChars[j * 2] = HexArray[value >>> 4];
			HexChars[j * 2 + 1] = HexArray[value & 0x0F];
		}
		return new String(HexChars);
	}

	private static byte computeCheckSum(byte...bytes){
		byte checkSum = (byte)(0xFF);
		for(byte oneByte:bytes)
			checkSum^=oneByte;
		return checkSum;
    }
	
	public static boolean bootloader(){
		AbsoluteSleep(1000);	
		smAppLog.d("Uart downloadPin set = 1");
		byte[] bytes = new byte[6];
		bytes[0] = 0x0A;
		bytes[1] = 0x00;
		bytes[2] = 0x00;
		bytes[3] = 0x00;
		bytes[4] = 0x00;
		byte checkSum = computeCheckSum(bytes[0],bytes[1],bytes[2],bytes[3],bytes[4]);
		bytes[5] = checkSum;
		SportInterface.sendBytes(bytes, 6);

		try {
			byte [] retBytes = new byte[6];
			retBytes = SportInterface.catchNBytes(7);
			result = ByteArraryToHexString(retBytes);
		} catch (NullPointerException e) {
			smAppLog.e(e.toString());
			return false;
		} catch (TimeoutException e) {
			smAppLog.e(e.toString());
			return false;
		}
		return true;
	}
	
	public static boolean Init() {
		smAppLog.d("App layer Setp 1 Cmd Init ");
		SportInterface.sendBytes(INIT, 1);
		if(Nack()) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public static boolean ReadUnlock(){
		smAppLog.d("App layer Setp Unlock Cmd Unlock ");
		SportInterface.sendBytes(UNLOCK, 2);
		if(Nack()) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public static boolean ReadProtect(){
		smAppLog.d("App layer Setp Lock Cmd Lock ");
		SportInterface.sendBytes(PROTECT, 2);
		if(Nack()) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public static boolean Get() {
		smAppLog.d("App layer Setp 2 Cmd Get");
		SportInterface.sendBytes(GET, 2);
		smAppLog.d("App layer Setp 2-1 Cmd Get (ACK ?)");
		if(Nack()) {
			return false;
		}
		else {
			return true;
		}
	}

	public static boolean Erase() {
		smAppLog.d("App layer Setp 3 Cmd Erase "); 
		SportInterface.sendBytes(ERASE, 2);
		if(Nack()) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public static boolean EraseAll(){
		smAppLog.d("App layer Setp 3-1 Erase Global (Really Delete History Data)");
		SportInterface.sendBytes(ERASE_GLOBAL, 2);
		if(Nack()) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public static boolean WriteMemory(String RomWritePath,String fileName) {
		try {
			smAppLog.d("App layer Setp 4 File cut in block (256 byte Buffer) ");
			InputStream is = new FileInputStream(RomWritePath+fileName);
			byte[] buffer = new byte[256];
			int length=0; 
			int j=0;  
			smAppLog.d("App layer  Setp 5 --------------Write Memory----------------");
			smAppLog.d("Start Flash Memory Address "+ByteArraryToHexString(flashStartMemoryAddress));

			while ((length = is.read(buffer)) > 0)
			{ // 演算資料
				smAppLog.d("This Loop Start Address "+ByteArraryToHexString(TempAddress));
				smAppLog.d("loop counter = "+j);  j++;
				smAppLog.d("this loop data length = "+length);
				//data[N] checksum
				byte len = (byte)(length-1);
				byte[] WriteLoopData = new byte[length+2];
				WriteLoopData[0] = len;
				smAppLog.d("App layer Setp 5-1 checksum data buffer stream (N+2) byte");
				for(int k=0;k<length;k++) {
					WriteLoopData[k+1]=buffer[k];
				}
				WriteLoopData[length+1]=CheckSum(len,buffer, length);
				smAppLog.d("App layer Setp 5-2 Write Cmd");
				SportInterface.sendBytes(WRITE_MEMORY, 2);
				// ACK or Nack
				smAppLog.d("App layer Setp 5-3 Write Cmd (ACK ?)" );	
				if(Nack()){
					return false;
				}
				
				// 指向記憶體 point to memory address
				smAppLog.d("App layer Setp 5-4 Point to Start Memory Address "+ByteArraryToHexString(TempAddress) );
				byte AddressCS = CheckSum(TempAddress, 4);  // memory cs 
				byte[] WriteAddress = {TempAddress[0],TempAddress[1],TempAddress[2],TempAddress[3],AddressCS};
				smAppLog.d(" address + checksum = "+ByteArraryToHexString(WriteAddress) );
				SportInterface.sendBytes(WriteAddress, 5);	
				// ACK or Nack
				smAppLog.d("App layer Setp 5-5 Point to Start Memory Address (ACK ?)" );
				if(Nack()){
					return false;
				}
				// 資料寫入 Data Really Write
				smAppLog.d("App layer Setp 5-6 Really Write Data Stream " );
				SportInterface.sendBytes(WriteLoopData, length+2);
				// ACK or Nack
				smAppLog.d("App layer Setp 5-7 Really Write Data Stream (ACK ?)" );
				SystemClock.sleep(100);
				if(Nack()){
					return false;
				}
				// 推算下個記憶位置 Next Memory Address
				if(length==256){
					TempAddress[2]+=1;// TempMemoryAddress + 256 byte
				}
				else{
					TempAddress[3]+=length;
				}
			}//end of while loop
			is.close();
			smAppLog.d("File copy final Address "+ByteArraryToHexString(TempAddress));
		} catch (Exception e) {
			smAppLog.e("InputStream = failed");
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
