package endexcase.scanmachine.uart;

import endexcase.scanmachine.util.AppLog;
import endexcase.scanmachine.util.Hex;

public class EndexScanProtocols {
	private static final AppLog smAppLog = new AppLog("uart",EndexScanProtocols.class);
	
	public static final int RESPONSE_DETECT_INTERVAL = (int)(0.5*1000);// in mills
	public static final int RESPONSE_TIMEOUT = 5*1000;// in mills

	public static final byte EMPTY_BYTES = (byte)0x00;

	// Command type
	public static final byte COMMAND_TYPE_BYTE = (byte)0x09;
	public static final byte COMMAND_TYPE_WORD = (byte)0x0A;
	public static final byte COMMAND_TYPE_DWORD = (byte)0x0C; //生產數量:LabQtyL_、LabQtyH_  標籤數量:RsvQtyL_、RsvQtyH_
	public static final byte COMMAND_TYPE_ECHO = (byte)0x03;
	public static final byte COMMAND_TYPE_UNKNOW = (byte)0x00;
	
	// WORD command IDs
	public static final int WORD_COMMAND_NULL = 0;
	public static final int WORD_COMMAND_TotalRunTimeHourL_ = 1;
	public static final int WORD_COMMAND_TotalRunTimeHourH_ = 2;
	public static final int WORD_COMMAND_TotalRunTimeSec_ = 3; //unused
	public static final int WORD_COMMAND_LabQtyL_ = 4;
	public static final int WORD_COMMAND_LabQtyH_ = 5;
	public static final int WORD_COMMAND_RsvQtyL_ = 6;
	public static final int WORD_COMMAND_RsvQtyH_ = 7;
	public static final int WORD_COMMAND_CnySetSpd_ = 8;
	public static final int WORD_COMMAND_BotSepaSetSpd_ = 9;
	public static final int WORD_COMMAND_LabSpd_ = 10;
	public static final int WORD_COMMAND_CvcState_ = 11;
	public static final int WORD_COMMAND_BotDiameter_ = 12;
	public static final int WORD_COMMAND_BotDiameterCnyCnt_ = 13;
	public static final int WORD_COMMAND_LabLengthL_ = 14;
	public static final int WORD_COMMAND_LabLengthR_ = 15;
	public static final int WORD_COMMAND_LabLengthStepL_ = 16;
	public static final int WORD_COMMAND_LabLengthStepR_ = 17;
	public static final int WORD_COMMAND_LabFeedSpdL_ = 18;
	public static final int WORD_COMMAND_LabFeedSpdR_ = 19;
	public static final int WORD_COMMAND_LabLeaveLengthL_ = 20;
	public static final int WORD_COMMAND_LabLeaveLengthR_ = 21;
	public static final int WORD_COMMAND_LabPositionL_ = 22;
	public static final int WORD_COMMAND_LabPositionR_ = 23;
	public static final int WORD_COMMAND_PaperThoughtL_ = 24;
	public static final int WORD_COMMAND_PaperThoughtR_ = 25;
	public static final int WORD_COMMAND_PaperSetL_ = 26;
	public static final int WORD_COMMAND_PaperSetR_ = 27;
	public static final int WORD_COMMAND_PrintTime_ = 28;
	public static final int WORD_COMMAND_FixPositionType_ = 29;
	public static final int WORD_COMMAND_TypeHasten_ = 30;
	public static final int WORD_COMMAND_TypeAccelerate_ = 31;
	public static final int WORD_COMMAND_ObjSenToPresDist_ = 32;
	public static final int WORD_COMMAND_V1SD_ = 33;
	public static final int WORD_COMMAND_V1ED_ = 34;
	public static final int WORD_COMMAND_BacklogOnDelay_ = 35;
	public static final int WORD_COMMAND_BacklogOffDelay_ = 36;
	public static final int WORD_COMMAND_DetectTpye_Select_ = 37;
	public static final int WORD_COMMAND_TypeChk_ = 38;
	public static final int WORD_COMMAND_S1TopSpd_ = 39;
	public static final int WORD_COMMAND_S2TopSpd_ = 40;
	public static final int WORD_COMMAND_S1StartSpd_ = 41;
	public static final int WORD_COMMAND_S2StartSpd_ = 42;
	public static final int WORD_COMMAND_S1StopSpd_ = 43;
	public static final int WORD_COMMAND_S2StopSpd_ = 44;
	public static final int WORD_COMMAND_S1UpRate_ = 45;
	public static final int WORD_COMMAND_S2UpRate_ = 46;
	public static final int WORD_COMMAND_S1UpScale_ = 47;
	public static final int WORD_COMMAND_S2UpScale_ = 48;
	public static final int WORD_COMMAND_S1DnRate_ = 49;
	public static final int WORD_COMMAND_S2DnRate_ = 50;
	public static final int WORD_COMMAND_S1DnScale_ = 51;
	public static final int WORD_COMMAND_S2DnScale_ = 52;
	public static final int WORD_COMMAND_S3TopSpd_ = 53;
	public static final int WORD_COMMAND_S3StartSpd_ = 54;
	public static final int WORD_COMMAND_S3StopSpd_ = 55;
	public static final int WORD_COMMAND_S3UpRate_ = 56;
	public static final int WORD_COMMAND_S3UpScale_ = 57;
	public static final int WORD_COMMAND_S3DnRate_ = 58;
	public static final int WORD_COMMAND_S3DnScale_ = 59;
	public static final int WORD_COMMAND_ObjSenToPeelDistL_ = 60;
	public static final int WORD_COMMAND_ObjSenToPeelDistR_ = 61;
	public static final int WORD_COMMAND_LabSenToPeelDistL_ = 62;
	public static final int WORD_COMMAND_LabSenToPeelDistR_ = 63;
	public static final int WORD_COMMAND_ConveyGearDiam_ = 64;
	public static final int WORD_COMMAND_LabelWheelDiamL_ = 65;
	public static final int WORD_COMMAND_LabelWheelDiamR_ = 66;
	public static final int WORD_COMMAND_RollPasteWheelDiam_ = 67;
	public static final int WORD_COMMAND_RollPasteMaGearNum_ = 68;
	public static final int WORD_COMMAND_RollPasteSlvGearNum_ = 69;
	public static final int WORD_COMMAND_RollPasteEncoderRes_ = 70;
	public static final int WORD_COMMAND_ConveyEncoderRes_ = 71;
	public static final int WORD_COMMAND_LimitRunTime_ = 72;
	public static final int WORD_COMMAND_MachineType_ = 73;
	public static final int WORD_COMMAND_BarCodeChk_ = 74;
	public static final int WORD_COMMAND_BotDimChk_ = 75;
	public static final int WORD_COMMAND_RecallMemory_ = 76;
	public static final int WORD_COMMAND_Show2_ = 77;
	public static final int WORD_COMMAND_SecretCode1_ = 78;
	public static final int WORD_COMMAND_SecretCode2_ = 79;
	public static final int WORD_COMMAND_SecretCode3_ = 80;
	public static final int WORD_COMMAND_SecretCode4_ = 81;
	public static final int WORD_COMMAND_PresPasteRange_ = 82;
	public static final int WORD_COMMAND_AirBlowTime_ = 83;
	public static final int WORD_COMMAND_AlarmCount_ = 84;
	public static final int WORD_COMMAND_MaxLabMiss_ = 85;
	public static final int WORD_COMMAND_LabSensorSet_ = 86;
	public static final int WORD_COMMAND_BotSensorWidth_ = 87;
	public static final int WORD_COMMAND_LabSensorWidth_ = 88;
	public static final int WORD_COMMAND_BottleSpd_ = 89;
	public static final int WORD_COMMAND_TestInput_ = 90;
	public static final int WORD_COMMAND_TestOutput_ = 91;
	public static final int WORD_COMMAND_PresPasteSpeed_ = 92;
	public static final int WORD_COMMAND_GatewaySoftwareYear_ = 93;
	public static final int WORD_COMMAND_GatewaySoftwareDate_ = 94;
	public static final int WORD_COMMAND_LabGapL_ = 95;
	public static final int WORD_COMMAND_LabGapR_ = 96;
	public static final int WORD_COMMAND_LabGapStepL_ = 97;
	public static final int WORD_COMMAND_LabGapStepR_ = 98;
	public static final int WORD_COMMAND_FixPointPasteEnable_ = 99;
	public static final int WORD_COMMAND_RollPasteEnable_ = 100;
	public static final int WORD_COMMAND_SromIDChk_ = 101;
	public static final int WORD_COMMAND_SysPage_ = 102;
	public static final int WORD_COMMAND_Show1_ = 103;
	public static final int WORD_COMMAND_BootType_ = 104;
	public static final int WORD_COMMAND_BotSeparateType_ = 105;
	public static final int WORD_COMMAND_Slave1SoftwareVersion_ = 106;
	public static final int WORD_COMMAND_Slave1SoftwareYear_ = 107;
	public static final int WORD_COMMAND_Slave1SoftwareDate_ = 108;
	public static final int WORD_COMMAND_SaveMemory_ = 109;
	public static final int WORD_COMMAND_LanguageMode_ = 110;
	public static final int WORD_COMMAND_PresPasteDist_ = 111;
	public static final int WORD_COMMAND_DebugMode_ = 112;
	public static final int WORD_COMMAND_MasterSoftwareVersion_ = 113;
	public static final int WORD_COMMAND_MasterSoftwareYear_ = 114;
	public static final int WORD_COMMAND_MasterSoftwareDate_ = 115;
	public static final int WORD_COMMAND_Slave2SoftwareVersion_ = 116;
	public static final int WORD_COMMAND_Slave2SoftwareYear_ = 117;
	public static final int WORD_COMMAND_Slave2SoftwareDate_ = 118;
	public static final int WORD_COMMAND_EndWord_ = 119;


	// BYTE command IDs
	public static final int BYTE_COMMAND_ADDR15 = 15;
	public static final int BYTE_COMMAND_ADDR15_LabEnable_ = 0;
	public static final int BYTE_COMMAND_ADDR15_BotSeparateEnable_ = 1;
	public static final int BYTE_COMMAND_ADDR15_CnyChkOnOff_ = 2;
	public static final int BYTE_COMMAND_ADDR15_TypingEnable_ = 3;
	public static final int BYTE_COMMAND_ADDR15_AutoManual_ = 4;
	public static final int BYTE_COMMAND_ADDR15_PaperSetEnable_ = 5;
	public static final int BYTE_COMMAND_ADDR15_TypeHeaterOnOff_ = 6;
	public static final int BYTE_COMMAND_ADDR15_BarCodeOnOff_ = 7;
	public static final int BYTE_COMMAND_ADDR16 = 16;
	public static final int BYTE_COMMAND_ADDR16_TypeChkOnOff_ = 0;
	public static final int BYTE_COMMAND_ADDR16_BotDimChkOnOff_ = 1;
	public static final int BYTE_COMMAND_ADDR16_LabEnable2_ = 2;
	public static final int BYTE_COMMAND_ADDR16_TypingEnable2_ = 3;
	public static final int BYTE_COMMAND_ADDR16_BotAutoMesEnable_ = 4;
	public static final int BYTE_COMMAND_ADDR16_LabSenLAutoMesEnable_ = 5;
	public static final int BYTE_COMMAND_ADDR16_LabSenRAutoMesEnable_ = 6;
	public static final int BYTE_COMMAND_ADDR16_LabLenLAutoMesEnable_ = 7;
	public static final int BYTE_COMMAND_ADDR17 = 17;
	public static final int BYTE_COMMAND_ADDR17_LabLenRAutoMesEnable_ = 0;
	public static final int BYTE_COMMAND_ADDR17_EditEnable_ = 1;
	public static final int BYTE_COMMAND_ADDR17_MemoryExist_ = 2;
	public static final int BYTE_COMMAND_ADDR17_BalenceEnable_ = 3;
	public static final int BYTE_COMMAND_ADDR17_QualifyEnable_ = 4;
	public static final int BYTE_COMMAND_ADDR17_SysTypeOn_ = 5;
	public static final int BYTE_COMMAND_ADDR17_SysBotDropOn_ = 6;
	public static final int BYTE_COMMAND_ADDR17_SysLabMissHalt_ = 7;
	public static final int BYTE_COMMAND_ADDR18 = 18;
	public static final int BYTE_COMMAND_ADDR18_AutoBotSeparate_ = 0;
	public static final int BYTE_COMMAND_ADDR18_StopAnyError_ = 1;
	public static final int BYTE_COMMAND_ADDR18_NoRibbonHalt_ = 2;
	public static final int BYTE_COMMAND_ADDR18_LabelTestModeEnable_ = 3;
	public static final int BYTE_COMMAND_ADDR18_AutoMesAllEnable_ = 4;
	public static final int BYTE_COMMAND_ADDR18_EditLabQtyEnable_ = 5;
	public static final int BYTE_COMMAND_ADDR18_RunStopKey_ = 6;
	public static final int BYTE_COMMAND_ADDR18_EditRsvQtyEnable_ = 7;
	public static final int BYTE_COMMAND_ADDR19 = 19;
	public static final int BYTE_COMMAND_ADDR19_SwEdit_ = 0;
	public static final int BYTE_COMMAND_ADDR19_SwLogIn_ = 1;
	public static final int BYTE_COMMAND_ADDR19_Balence_ = 2;
	public static final int BYTE_COMMAND_ADDR19_Clear_ = 3;
	public static final int BYTE_COMMAND_ADDR19_ChangeLine_ = 4;
	public static final int BYTE_COMMAND_ADDR19_AirBumpCtl_ = 5;
	public static final int BYTE_COMMAND_ADDR19_TypeCtl_ = 6;
	public static final int BYTE_COMMAND_ADDR19_ConVeyCtl_ = 7;
	public static final int BYTE_COMMAND_ADDR20 = 20;
	public static final int BYTE_COMMAND_ADDR20_LabFeedCtl_ = 0;
	public static final int BYTE_COMMAND_ADDR20_BotWheelCtl_ = 1;
	public static final int BYTE_COMMAND_ADDR20_ClearAllMem_ = 2;
	public static final int BYTE_COMMAND_ADDR20_PasswordCtl_ = 3;
	public static final int BYTE_COMMAND_ADDR20_CheckRollPaste_ = 4;
	public static final int BYTE_COMMAND_ADDR20_MainBoardIONoMatch_ = 5;
	public static final int BYTE_COMMAND_ADDR20_DetectTpye_SelDevice_ = 6;
	public static final int BYTE_COMMAND_ADDR20_DetectTpye_Switch_ =7;
	public static final int BYTE_COMMAND_ADDR21 = 21;
	public static final int BYTE_COMMAND_ADDR22 = 22;
	public static final int BYTE_COMMAND_ADDR23 = 23;
	public static final int BYTE_COMMAND_ADDR24 = 24;
	public static final int BYTE_COMMAND_ADDR25 = 25;
	public static final int BYTE_COMMAND_ADDR26 = 26;
	public static final int BYTE_COMMAND_ADDR27 = 27;
	public static final int BYTE_COMMAND_ADDR28 = 28;
	public static final int BYTE_COMMAND_ADDR29 = 29;
	public static final int BYTE_COMMAND_ADDR29_MemData01_ = 0;
	public static final int BYTE_COMMAND_ADDR29_MemData02_ = 1;
	public static final int BYTE_COMMAND_ADDR29_MemData03_ = 2;
	public static final int BYTE_COMMAND_ADDR29_MemData04_ = 3;
	public static final int BYTE_COMMAND_ADDR29_MemData05_ = 4;
	public static final int BYTE_COMMAND_ADDR29_MemData06_ = 5;
	public static final int BYTE_COMMAND_ADDR29_MemData07_ = 6;
	public static final int BYTE_COMMAND_ADDR29_MemData08_ = 7;
	public static final int BYTE_COMMAND_ADDR30 = 30;
	public static final int BYTE_COMMAND_ADDR30_MemData09_ = 0;
	public static final int BYTE_COMMAND_ADDR30_MemData10_ = 1;
	public static final int BYTE_COMMAND_ADDR30_MemData11_ = 2;
	public static final int BYTE_COMMAND_ADDR30_MemData12_ = 3;
	public static final int BYTE_COMMAND_ADDR30_MemData13_ = 4;
	public static final int BYTE_COMMAND_ADDR30_MemData14_ = 5;
	public static final int BYTE_COMMAND_ADDR30_MemData15_ = 6;
	public static final int BYTE_COMMAND_ADDR30_MemData16_ = 7;
	public static final int BYTE_COMMAND_ADDR31 = 31;
	public static final int BYTE_COMMAND_ADDR31_MemData17_ = 0;
	public static final int BYTE_COMMAND_ADDR31_MemData18_ = 1;
	public static final int BYTE_COMMAND_ADDR31_MemData19_ = 2;
	public static final int BYTE_COMMAND_ADDR31_MemData20_ = 3;
	public static final int BYTE_COMMAND_ADDR31_MemData21_ = 4;
	public static final int BYTE_COMMAND_ADDR31_MemData22_ = 5;
	public static final int BYTE_COMMAND_ADDR31_MemData23_ = 6;
	public static final int BYTE_COMMAND_ADDR31_MemData24_ = 7;
	public static final int BYTE_COMMAND_ADDR32 = 32;
	public static final int BYTE_COMMAND_ADDR32_MemData25_ = 0;
	public static final int BYTE_COMMAND_ADDR32_MemData26_ = 1;
	public static final int BYTE_COMMAND_ADDR32_MemData27_ = 2;
	public static final int BYTE_COMMAND_ADDR32_MemData28_ = 3;
	public static final int BYTE_COMMAND_ADDR32_MemData29_ = 4;
	public static final int BYTE_COMMAND_ADDR32_MemData30_ = 5;
	public static final int BYTE_COMMAND_ADDR32_MemData31_ = 6;
	public static final int BYTE_COMMAND_ADDR32_MemData32_ = 7;
	public static final int BYTE_COMMAND_ADDR33 = 33;
	public static final int BYTE_COMMAND_ADDR33_MemData33_ = 0;
	public static final int BYTE_COMMAND_ADDR33_MemData34_ = 1;
	public static final int BYTE_COMMAND_ADDR33_MemData35_ = 2;
	public static final int BYTE_COMMAND_ADDR33_MemData36_ = 3;
	public static final int BYTE_COMMAND_ADDR33_MemData37_ = 4;
	public static final int BYTE_COMMAND_ADDR33_MemData38_ = 5;
	public static final int BYTE_COMMAND_ADDR33_MemData39_ = 6;
	public static final int BYTE_COMMAND_ADDR33_MemData40_ = 7;
	public static final int BYTE_COMMAND_ADDR34 = 34;
	public static final int BYTE_COMMAND_ADDR34_MemData41_ = 0;
	public static final int BYTE_COMMAND_ADDR34_MemData42_ = 1;
	public static final int BYTE_COMMAND_ADDR34_MemData43_ = 2;
	public static final int BYTE_COMMAND_ADDR34_MemData44_ = 3;
	public static final int BYTE_COMMAND_ADDR34_MemData45_ = 4;
	public static final int BYTE_COMMAND_ADDR34_MemData46_ = 5;
	public static final int BYTE_COMMAND_ADDR34_MemData47_ = 6;
	public static final int BYTE_COMMAND_ADDR34_MemData48_ = 7;
	public static final int BYTE_COMMAND_ADDR35 = 35;
	public static final int BYTE_COMMAND_ADDR35_MemData49_ = 0;
	public static final int BYTE_COMMAND_ADDR35_MemData50_ = 1;
	
	//需判斷是否無重複的Command
	public static int mByteSendCnt=0;
	public static int mWordSendCnt=0;
	public static int mDWordSendCnt=0;
	public static int mNormalSendCnt=0;
	
	public static int mByteRecvCnt=-1;
	public static int mWordRecvCnt=-1;
	public static int mDWordRecvCnt=-1;

	public static byte[] getByteRequestCommand(int addr, byte data, int no, boolean val) {
		byte[] cmd = new byte[5];
		//cmd[0] = (byte) (COMMAND_TYPE_BYTE | ((mByteSendCnt<<6)&0xC0) );
		cmd[0] = (byte) (COMMAND_TYPE_BYTE | ((mNormalSendCnt<<6)&0xC0) );
		cmd[1] = (byte) ((addr>>8)&0xFF);
		cmd[2] = (byte) (addr&0xFF);
		
		if(!val)
			data &= ~(1 << no);
		else
			data |= 1 << no;
		
		cmd[3] = data;
		
		cmd[4] = computeCheckSum(cmd[0],cmd[1],cmd[2],cmd[3]);
		
		/*if(mByteSendCnt == 3)
			mByteSendCnt=0;
		else
			mByteSendCnt++;
		*/
		if(mNormalSendCnt == 3)
			mNormalSendCnt=0;
		else
			mNormalSendCnt++;
		return cmd;
	}
	
	public static byte[] getByteRequestCommand(int addr, byte data) {
		byte[] cmd = new byte[5];
		//cmd[0] = (byte) (COMMAND_TYPE_BYTE | ((mByteSendCnt<<6)&0xC0) );
		cmd[0] = (byte) (COMMAND_TYPE_BYTE | ((mNormalSendCnt<<6)&0xC0) );
		cmd[1] = (byte) ((addr>>8)&0xFF);
		cmd[2] = (byte) (addr&0xFF);
		cmd[3] = data;
		
		cmd[4] = computeCheckSum(cmd[0],cmd[1],cmd[2],cmd[3]);
		
		/*if(mByteSendCnt == 3)
			mByteSendCnt=0;
		else
			mByteSendCnt++;
		*/
		if(mNormalSendCnt == 3)
			mNormalSendCnt=0;
		else
			mNormalSendCnt++;
		return cmd;
	}
	
	public static byte[] getWordRequestCommand(int addr, int data) {
		byte[] cmd = new byte[6];
		//cmd[0] = (byte) (COMMAND_TYPE_WORD | ((mWordSendCnt<<6)&0xC0) );
		cmd[0] = (byte) (COMMAND_TYPE_WORD | ((mNormalSendCnt<<6)&0xC0) );
		cmd[1] = (byte) ((addr>>8)&0xFF);
		cmd[2] = (byte) (addr&0xFF);
		
		cmd[3] = (byte) ((data>>8)&0xFF);
		cmd[4] = (byte) (data&0xFF);
		
		cmd[5] = computeCheckSum(cmd[0],cmd[1],cmd[2],cmd[3],cmd[4]);
		
		/*
		if(mWordSendCnt == 3)
			mWordSendCnt=0;
		else
			mWordSendCnt++;
		*/
		if(mNormalSendCnt == 3)
			mNormalSendCnt=0;
		else
			mNormalSendCnt++;
		return cmd;
	}
	
	public static byte[] getDWordRequestCommand(int addr, int data) {
		byte[] cmd = new byte[8];
		cmd[0] = (byte) (COMMAND_TYPE_DWORD | ((mDWordSendCnt<<6)&0xC0) );
		cmd[1] = (byte) ((addr>>8)&0xFF);
		cmd[2] = (byte) (addr&0xFF);
		
		//data Max: 9999999
		if(data <= 9999999 && data >= 0) {
			cmd[3] = (byte) ((data>>24)&0xFF);
			cmd[4] = (byte) ((data>>16)&0xFF);
			cmd[5] = (byte) ((data>>8)&0xFF);
			cmd[6] = (byte) (data&0xFF);
		}
		
		cmd[8] = computeCheckSum(cmd[0],cmd[1],cmd[2],cmd[3],cmd[4],cmd[5],cmd[6]);
		
		if(mDWordSendCnt == 3)
			mDWordSendCnt=0;
		else
			mDWordSendCnt++;
		return cmd;
	}
	
	public static byte[] getEchoCommand() {
		byte[] cmd = new byte[1];
		cmd[0] = (byte) (COMMAND_TYPE_ECHO);

		return cmd;
	}
	
	public static byte checkCommandType(byte cmdType) {
		byte cmd = COMMAND_TYPE_UNKNOW;
		byte tmpCmd = COMMAND_TYPE_UNKNOW;
		int count = (cmdType >> 6) & 0x03;
		
		tmpCmd = (byte) (cmdType&0x3F);

		if(tmpCmd == COMMAND_TYPE_BYTE) {
			//if(count != mByteRecvCnt) {
				cmd = tmpCmd;
				mByteRecvCnt = count;
			//}
		}
		else if(tmpCmd == COMMAND_TYPE_WORD) {
			//if(count != mWordRecvCnt) {
				cmd = tmpCmd;
				mWordRecvCnt = count;
			//}
		}
		else if(tmpCmd == COMMAND_TYPE_DWORD) {
			//if(count != mDWordRecvCnt) {
				cmd = tmpCmd;
				mDWordRecvCnt = count;
			//}
		}

		return cmd;
	}
	

	public static byte[] getRequestCmdByteArray(byte cmdVarId,byte requestByte1,byte requestByte2,byte requestByte3,byte requestByte4){
		byte[] bytes = new byte[6];
		bytes[0] = cmdVarId;
		bytes[1] = requestByte1;
		bytes[2] = requestByte2;
		bytes[3] = requestByte3;
		bytes[4] = requestByte4;

		byte checkSum = computeCheckSum(bytes[0],bytes[1],bytes[2],bytes[3],bytes[4]);

		bytes[5] = checkSum;

		return bytes;
	}

	private static byte computeCheckSum(byte...bytes){
		byte checkSum = (byte)(0x00);
		for(byte oneByte:bytes)
			checkSum^=oneByte;
		return checkSum;
	}

	public static boolean isEchoCommand(byte val) {
		return (COMMAND_TYPE_ECHO==val?true:false);
	}
}
