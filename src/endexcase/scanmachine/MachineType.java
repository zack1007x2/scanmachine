package endexcase.scanmachine;

import android.util.Log;
import endexcase.scanmachine.database.DatabaseProxy;
import endexcase.scanmachine.uart.EndexScanProtocols;

/**
 * 保存Machine資料
 */
public class MachineType{
	public static final int KK806U = 0;
	public static final int CVC200U = 1;
	public static final int KK906U = 2;
	public static final int KK916U = 3;
	public static final int CVC300U = 4;
	public static final int CVC302U = 5;
	public static final int CVC330U = 6;
	public static final int KK926U = 7;
	public static final int CVC400U = 8;
	public static final int KK956U = 9;
	public static final int CVC430U = 10;
	public static final int KK996U = 11;
	public static final int CVC350U = 12;
	public static final int KK998U = 13;
	public static final int CVC310U = 14;
	public static final int KK936U = 15;
	public static final int CVC220U = 16;
	public static final int CVC210U = 17;
	public static final int CVC230U = 18;
	public static final int KK906UP = 19;
	public static final int KK916UP = 20;
	public static final int CVC300UP = 21;
	public static final int CVC330UP = 22;

	private MachineType(){
	}
	
	public static void setDefValue(int machine, DatabaseProxy db) {
		AppData appdata = AppData.getInstance();
		switch(machine) {
		case KK806U:
			int[] KK806U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 0, 0, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 20, 20, 0, 0, //4X
					15, 15, 4000, 500, 500, 7, 20, 0, 15, 7600, //5X
					7600, 23200, 23200, 8200, 4800, 4800, 0, 100, 100, 0, //6X
					770, 0, 0, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 0, //9X
					0xC118, 2, 0, 0, 0, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = KK806U_defVal[i];
			}
			break;
		case CVC200U:
			int[] CVC200U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 0, 0, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 20, 20, 0, 0, //4X
					15, 15, 4000, 500, 500, 7, 20, 0, 15, 7600, //5X
					7600, 23200, 23200, 8200, 4800, 4800, 0, 100, 100, 0, //6X
					770, 0, 1, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 0, //9X
					0xC118, 2, 0, 0, 0, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = CVC200U_defVal[i];
			}
			break;
		case KK906U:
			int[] KK906U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 95, 15, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 60, 60, 0, 0, //4X
					25, 25, 4000, 500, 500, 7, 20, 0, 15, 8500, //5X
					8500, 23500, 23500, 11200, 4800, 4800, 8200, 100, 100, 510, //6X
					1380, 0, 2, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 1, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = KK906U_defVal[i];
			}
			break;
		case KK916U:
			int[] KK916U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 95, 15, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 60, 60, 0, 0, //4X
					25, 25, 4000, 500, 500, 7, 20, 0, 15, 8500, //5X
					8500, 23500, 23500, 11200, 4800, 4800, 8200, 100, 100, 510, //6X
					1380, 0, 3, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 1, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = KK916U_defVal[i];
			}
			break;
		case CVC300U:
			int[] CVC300U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 95, 15, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 60, 60, 0, 0, //4X
					25, 25, 4000, 500, 500, 7, 20, 0, 15, 8500, //5X
					8500, 23500, 23500, 11200, 4800, 4800, 8200, 100, 100, 510, //6X
					1380, 0, 4, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 1, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = CVC300U_defVal[i];
			}
			break;
		case CVC302U:
			int[] CVC302U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 95, 15, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 60, 60, 0, 0, //4X
					25, 25, 4000, 500, 500, 7, 20, 0, 15, 8500, //5X
					8500, 23500, 23500, 11200, 4800, 4800, 8200, 100, 100, 510, //6X
					1380, 0, 5, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 1, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = CVC302U_defVal[i];
			}
			break;
		case CVC330U:
			int[] CVC330U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 95, 15, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 60, 60, 0, 0, //4X
					25, 25, 4000, 500, 500, 7, 20, 0, 15, 8500, //5X
					8500, 23500, 23500, 11200, 4800, 4800, 8200, 100, 100, 510, //6X
					1380, 0, 6, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 1, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = CVC330U_defVal[i];
			}
			break;
		case KK926U:
			int[] KK926U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0303, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 23000, 0, 0, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 25, 25, 0, 0, //4X
					20, 20, 4000, 500, 500, 7, 20, 0, 15, 12800, //5X
					12800, 23100, 23100, 11200, 4800, 4800, 5200, 100, 100, 540, //6X
					1550, 0, 7, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 9999, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 3000, 0, 0, 50, 50, 5, 5, 0, 0, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					115, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = KK926U_defVal[i];
			}
			break;
		case CVC400U:
			int[] CVC400U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0303, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 23000, 0, 0, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 25, 25, 0, 0, //4X
					20, 20, 4000, 500, 500, 7, 20, 0, 15, 12800, //5X
					12800, 23100, 23100, 11200, 4800, 4800, 5200, 100, 100, 540, //6X
					1550, 0, 8, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 9999, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 3000, 0, 0, 50, 50, 5, 5, 0, 0, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					115, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = CVC400U_defVal[i];
			}
			break;
		case KK956U:
			int[] KK956U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0303, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 23000, 0, 0, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 25, 25, 0, 0, //4X
					20, 20, 4000, 500, 500, 7, 20, 0, 15, 12800, //5X
					12800, 23100, 23100, 11200, 4800, 4800, 5200, 100, 100, 540, //6X
					1550, 0, 9, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 9999, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 3000, 0, 0, 50, 50, 5, 5, 0, 1, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					115, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = KK956U_defVal[i];
			}
			break;
		case CVC430U:
			int[] CVC430U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0303, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 23000, 0, 0, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 25, 25, 0, 0, //4X
					20, 20, 4000, 500, 500, 7, 20, 0, 15, 12800, //5X
					12800, 23100, 23100, 11200, 4800, 4800, 5200, 100, 100, 540, //6X
					1550, 0, 10, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 9999, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 3000, 0, 0, 50, 50, 5, 5, 0, 1, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					115, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = CVC430U_defVal[i];
			}
			break;
		case KK996U:
			int[] KK996U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 0, 0, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 20, 20, 0, 0, //4X
					15, 15, 8000, 500, 500, 0, 500, 0, 500, 700, //5X
					700, 23200, 23200, 7500, 4800, 4800, 5400, 25, 15, 600, //6X
					812, 0, 11, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 1, //9X
					0xC118, 2, 0, 0, 0, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = KK996U_defVal[i];
			}
			break;
		case CVC350U:
			int[] CVC350U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 0, 0, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 20, 20, 0, 0, //4X
					15, 15, 8000, 500, 500, 0, 500, 0, 500, 700, //5X
					700, 23200, 23200, 7500, 4800, 4800, 5400, 25, 15, 600, //6X
					812, 0, 12, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 1, //9X
					0xC118, 2, 0, 0, 0, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = CVC350U_defVal[i];
			}
			break;
		case KK998U:
			int[] KK998U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 0, 0, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 20, 20, 0, 0, //4X
					15, 15, 8000, 500, 500, 0, 500, 0, 500, 700, //5X
					700, 23200, 23200, 7500, 4800, 4800, 5400, 25, 15, 600, //6X
					812, 0, 13, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 1, //9X
					0xC118, 2, 0, 0, 0, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = KK998U_defVal[i];
			}
			break;
		case CVC310U:
			int[] CVC310U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 0, 0, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 60, 60, 0, 0, //4X
					25, 25, 8000, 500, 500, 0, 500, 0, 500, 8500, //5X
					8500, 23500, 23500, 11200, 4800, 4800, 8200, 100, 100, 520, //6X
					1370, 0, 14, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 1, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = CVC310U_defVal[i];
			}
			break;
		case KK936U:
			int[] KK936U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0303, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 0, 0, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 25, 25, 0, 0, //4X
					20, 20, 8000, 500, 500, 0, 500, 0, 500, 25300, //5X
					24400, 24500, 23200, 11200, 4800, 4800, 0, 100, 100, 0, //6X
					1580, 0, 15, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 0, //9X
					0xC118, 2, 0, 0, 0, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = KK936U_defVal[i];
			}
			break;
		case CVC220U:
			int[] CVC220U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0303, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 0, 0, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 25, 25, 0, 0, //4X
					20, 20, 8000, 500, 500, 0, 500, 0, 500, 25300, //5X
					24400, 24500, 23200, 11200, 4800, 4800, 0, 100, 100, 0, //6X
					1580, 0, 16, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 0, //9X
					0xC118, 2, 0, 0, 0, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = CVC220U_defVal[i];
			}
			break;
		case CVC210U:
			int[] CVC210U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 0, 0, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 20, 20, 0, 0, //4X
					15, 15, 8000, 500, 500, 0, 500, 0, 500, 24400, //5X
					24400, 23200, 23200, 11200, 4800, 4800, 0, 100, 100, 0, //6X
					1580, 0, 17, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 0, //9X
					0xC118, 2, 0, 0, 0, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = CVC210U_defVal[i];
			}
			break;
		case CVC230U:
			int[] CVC230U_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0303, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 0, 15, //2X
					0, 0, 0, 0, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 25, 25, 0, 0, //4X
					20, 20, 8000, 500, 500, 0, 500, 0, 500, 3050, //5X
					7650, 28500, 30000, 26000, 4800, 4800, 0, 100, 100, 0, //6X
					1530, 0, 18, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 0, 0, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = CVC230U_defVal[i];
			}
			break;
		case KK906UP:
			int[] KK906UP_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 1, 15, //2X
					0, 0, 80, 15, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 60, 60, 0, 0, //4X
					25, 25, 4000, 500, 500, 7, 20, 0, 15, 8500, //5X
					8500, 23500, 23500, 11200, 4800, 4800, 8200, 100, 100, 510, //6X
					1380, 0, 19, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 1, 1, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = KK906UP_defVal[i];
			}
			break;
		case KK916UP:
			int[] KK916UP_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 1, 15, //2X
					0, 0, 80, 15, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 60, 60, 0, 0, //4X
					25, 25, 4000, 500, 500, 7, 20, 0, 15, 8500, //5X
					8500, 23500, 23500, 11200, 4800, 4800, 8200, 100, 100, 510, //6X
					1380, 0, 20, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 1, 1, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = KK916UP_defVal[i];
			}
			break;
		case CVC300UP:
			int[] CVC300UP_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 1, 15, //2X
					0, 0, 80, 15, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 60, 60, 0, 0, //4X
					25, 25, 4000, 500, 500, 7, 20, 0, 15, 8500, //5X
					8500, 23500, 23500, 11200, 4800, 4800, 8200, 100, 100, 510, //6X
					1380, 0, 21, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 1, 1, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = CVC300UP_defVal[i];
			}
			break;
		case CVC330UP:
			int[] CVC330UP_defVal = { 0, 0, 0, 0, 0, 0, 0, 0, 200, 150, //0X
					0, 0x0300, 1000, 100, 400, 400, 141, 300, 0, 0, 0, //1X
					0, 0, 0, 30, 30, 60, 60, 15, 1, 15, //2X
					0, 0, 80, 15, 15, 15, 0, 5, 4000, 4000, //3X
					150, 150, 150, 150, 7, 7, 60, 60, 0, 0, //4X
					25, 25, 4000, 500, 500, 7, 20, 0, 15, 8500, //5X
					8500, 23500, 23500, 11200, 4800, 4800, 8200, 100, 100, 510, //6X
					1380, 0, 22, 600, 0, 0, 0, 300, 0, 0, //7X
					1203, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8X
					0, 0, 0, 0, 50, 50, 5, 5, 1, 1, //9X
					0xC118, 2, 0, 0, 1, 0, 0, 0, 1, 1, //10X
					0, 0, 0, 0, 0, 0, 0, 0, 0}; //11X
			
			for(int i=0; i<EndexScanProtocols.WORD_COMMAND_EndWord_; i++) {
				appdata.mWordPara[i] = CVC330UP_defVal[i];
			}
			break;
		}
		
		setDefByteValue(db);
		
		return;
	}
	
	private static void setDefByteValue(DatabaseProxy db) {
		AppData appdata = AppData.getInstance();
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR15, EndexScanProtocols.BYTE_COMMAND_ADDR15_LabEnable_, true);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR15, EndexScanProtocols.BYTE_COMMAND_ADDR15_BotSeparateEnable_, true);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR15, EndexScanProtocols.BYTE_COMMAND_ADDR15_CnyChkOnOff_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR15, EndexScanProtocols.BYTE_COMMAND_ADDR15_TypingEnable_, true);
		
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR16, EndexScanProtocols.BYTE_COMMAND_ADDR16_BotDimChkOnOff_, true);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR16, EndexScanProtocols.BYTE_COMMAND_ADDR16_LabEnable2_, true);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR16, EndexScanProtocols.BYTE_COMMAND_ADDR16_TypingEnable2_, true);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR16, EndexScanProtocols.BYTE_COMMAND_ADDR16_BotAutoMesEnable_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR16, EndexScanProtocols.BYTE_COMMAND_ADDR16_LabSenLAutoMesEnable_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR16, EndexScanProtocols.BYTE_COMMAND_ADDR16_LabSenRAutoMesEnable_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR16, EndexScanProtocols.BYTE_COMMAND_ADDR16_LabLenLAutoMesEnable_, false);
		
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR17, EndexScanProtocols.BYTE_COMMAND_ADDR17_LabLenRAutoMesEnable_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR17, EndexScanProtocols.BYTE_COMMAND_ADDR17_SysLabMissHalt_, true);
		
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR18, EndexScanProtocols.BYTE_COMMAND_ADDR18_NoRibbonHalt_, true);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR18, EndexScanProtocols.BYTE_COMMAND_ADDR18_AutoMesAllEnable_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR18, EndexScanProtocols.BYTE_COMMAND_ADDR18_EditLabQtyEnable_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR18, EndexScanProtocols.BYTE_COMMAND_ADDR18_RunStopKey_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR18, EndexScanProtocols.BYTE_COMMAND_ADDR18_EditRsvQtyEnable_, false);

		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR20, EndexScanProtocols.BYTE_COMMAND_ADDR20_CheckRollPaste_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR20, EndexScanProtocols.BYTE_COMMAND_ADDR20_MainBoardIONoMatch_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR20, EndexScanProtocols.BYTE_COMMAND_ADDR20_DetectTpye_SelDevice_, true);
		
		db.getMachineMemData(appdata.mWordPara[EndexScanProtocols.WORD_COMMAND_MachineType_]);
	}
}
