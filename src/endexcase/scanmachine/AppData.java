package endexcase.scanmachine;

import endexcase.scanmachine.uart.EndexScanProtocols;
import endexcase.scanmachine.util.Consts;

/**
 * 保存App的共用資料
 */
public class AppData{
	private static final AppData smAppData = new AppData();
	
	//Language Setting
	public static final int LANGUAGE_ENGLISH = 0;
	public static final int LANGUAGE_CHINESE = 1;
	
	//Error Message ID
	public static final int ERROR_MESSAGE_ID_64 = 64;
	public static final int ERROR_MESSAGE_ID_65 = 65;
	public static final int ERROR_MESSAGE_ID_66 = 66;
	public static final int ERROR_MESSAGE_ID_67 = 67;
	public static final int ERROR_MESSAGE_ID_68 = 68;
	public static final int ERROR_MESSAGE_ID_69 = 69;
	public static final int ERROR_MESSAGE_ID_70 = 70;
	public static final int ERROR_MESSAGE_ID_74 = 74;
	public static final int ERROR_MESSAGE_ID_75 = 75;
	public static final int ERROR_MESSAGE_ID_76 = 76;
	public static final int ERROR_MESSAGE_ID_77 = 77;
	public static final int ERROR_MESSAGE_ID_81 = 81;
	public static final int ERROR_MESSAGE_ID_83 = 83;
	public static final int ERROR_MESSAGE_ID_85 = 85;
	
	//WORD Parameters
	public int[] mWordPara = new int[Consts.MACHINE_WORD_PARAMETER_SIZE];
	
	/*
	public int mTotalRunTimeHourL = 0;
	public int mTotalRunTimeHourH = 0;
	public int mTotalRunTimeSec = 0;
	public int mLabQtyL = 0;
	public int mLabQtyH = 0;
	public int mRsvQtyL = 0;
	public int mRsvQtyH = 0;
	public int mCnySetSpd = 0;
	public int mBotSepaSetSpd = 0;
	public int mLabSpd = 0;
	public int mCvcState = 0;
	public int mBotDiameter = 0;
	public int mBotDiameterCnyCnt = 0;
	public int mLabLengthL = 0;
	public int mLabLengthR = 0;
	public int mLabLengthStepL = 0;
	public int mLabLengthStepR = 0;
	public int mLabFeedSpdL = 0;
	public int mLabFeedSpdR = 0;
	public int mLabLeaveLengthL = 0;
	public int mLabLeaveLengthR = 0;
	public int mLabPositionL = 0;
	public int mLabPositionR = 0;
	public int mPaperThoughtL = 0;
	public int mPaperThoughtR = 0;
	public int mPaperSetL = 0;
	public int mPaperSetR = 0;
	public int mPrintTime = 0;
	public int mFixPositionType = 0;
	public int mTypeHasten = 0;
	public int mTypeAccelerate = 0;
	public int mObjSenToPresDist = 0;
	public int mV1SD = 0;
	public int mV1ED = 0;
	public int mBacklogOnDelay = 0;
	public int mBacklogOffDelay = 0;
	public int mDetectTpye_Select = 0;
	public int mTypeChk = 0;
	public int mS1TopSpd = 0;
	public int mS2TopSpd = 0;
	public int mS1StartSpd = 0;
	public int mS2StartSpd = 0;
	public int mS1StopSpd = 0;
	public int mS2StopSpd = 0;
	public int mS1UpRate = 0;
	public int mS2UpRate = 0;
	public int mS1UpScale = 0;
	public int mS2UpScale = 0;
	public int mS1DnRate = 0;
	public int mS2DnRate = 0;
	public int mS1DnScale = 0;
	public int mS2DnScale = 0;
	public int mS3TopSpd = 0;
	public int mS3StartSpd = 0;
	public int mS3StopSpd = 0;
	public int mS3UpRate = 0;
	public int mS3UpScale = 0;
	public int mS3DnRate = 0;
	public int mS3DnScale = 0;
	public int mObjSenToPeelDistL = 0;
	public int mObjSenToPeelDistR = 0;
	public int mLabSenToPeelDistL = 0;
	public int mLabSenToPeelDistR = 0;
	public int mConveyGearDiam = 0;
	public int mLabelWheelDiamL = 0;
	public int mLabelWheelDiamR = 0;
	public int mRollPasteWheelDiam = 0;
	public int mRollPasteMaGearNum = 0;
	public int mRollPasteSlvGearNum = 0;
	public int mRollPasteEncoderRes = 0;
	public int mConveyEncoderRes = 0;
	public int mLimitRunTime = 0;
	public int mMachineType = 0;
	public int mBarCodeChk = 0;
	public int mBotDimChk = 0;
	public int mRecallMemory = 0;
	public int mShow2 = 0;
	public int mSecretCode1 = 0;
	public int mSecretCode2 = 0;
	public int mSecretCode3 = 0;
	public int mSecretCode4 = 0;
	public int mPresPasteRange = 0;
	public int mAirBlowTime = 0;
	public int mAlarmCount = 0;
	public int mMaxLabMiss = 0;
	public int mLabSensorSet = 0;
	public int mBotSensorWidth = 0;
	public int mLabSensorWidth = 0;
	public int mBottleSpd = 0;
	public int mTestInput = 0;
	public int mTestOutput = 0;
	public int mPresPasteSpeed = 0;
	public int mGatewaySoftwareYear = 0;
	public int mGatewaySoftwareDate = 0;
	public int mLabGapL = 0;
	public int mLabGapR = 0;
	public int mLabGapStepL = 0;
	public int mLabGapStepR = 0;
	public int mFixPointPasteEnable = 0;
	public int mRollPasteEnable = 0;
	public int mSromIDChk = 0;
	public int mSysPage = 0;
	public int mShow1 = 0;
	public int mBootType = 0;
	public int mBotSeparateType = 0;
	public int mSlave1SoftwareVersion = 0;
	public int mSlave1SoftwareYear = 0;
	public int mSlave1SoftwareDate = 0;
	public int mSaveMemory = 0;
	public int mLanguageMode = 0;
	public int mPresPasteDist =0 ;
	public int mDebugMode = 0;
	public int mMasterSoftwareVersion = 0;
	public int mMasterSoftwareYear = 0;
	public int mMasterSoftwareDate = 0;
	public int mSlave2SoftwareVersion = 0;
	public int mSlave2SoftwareYear = 0;
	public int mSlave2SoftwareDate = 0;
	public int mEndWord = 0;
	*/
	public int mAddr15 = 0;
	public int mAddr16 = 0;
	public int mAddr17 = 0;
	public int mAddr18 = 0;
	public int mAddr19 = 0;
	public int mAddr20 = 0;
	public int mAddr21 = 0;
	public int mAddr22 = 0;
	public int mAddr23 = 0;
	public int mAddr24 = 0;
	public int mAddr25 = 0;
	public int mAddr26 = 0;
	public int mAddr27 = 0;
	public int mAddr28 = 0;
	public int mAddr29 = 0;
	public int mAddr30 = 0;
	public int mAddr31 = 0;
	public int mAddr32 = 0;
	public int mAddr33 = 0;
	public int mAddr34 = 0;
	public int mAddr35 = 0;
	
	public static AppData getInstance(){
		return smAppData;
	}

	private AppData(){
	}
	
	public boolean getBitData(int addr, int bitNo) {
		boolean result=false;
		switch(addr) {
		case EndexScanProtocols.BYTE_COMMAND_ADDR15:
			result = (((mAddr15>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR16:
			result = (((mAddr16>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR17:
			result = (((mAddr17>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR18:
			result = (((mAddr18>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR19:
			result = (((mAddr19>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR20:
			result = (((mAddr20>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR21:
			result = (((mAddr21>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR22:
			result = (((mAddr22>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR23:
			result = (((mAddr23>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR24:
			result = (((mAddr24>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR25:
			result = (((mAddr25>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR26:
			result = (((mAddr26>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR27:
			result = (((mAddr27>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR28:
			result = (((mAddr28>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR29:
			result = (((mAddr29>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR30:
			result = (((mAddr30>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR31:
			result = (((mAddr31>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR32:
			result = (((mAddr32>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR33:
			result = (((mAddr33>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR34:
			result = (((mAddr34>>bitNo)&0x01) == 1)?true:false;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR35:
			result = (((mAddr35>>bitNo)&0x01) == 1)?true:false;
			break;
		}
		return result;
	}
	
	public void setBitData(int addr, int bitNo, boolean val) {
		
		switch(addr) {
		case EndexScanProtocols.BYTE_COMMAND_ADDR15:
			if(!val)
				mAddr15 &= ~(1 << bitNo);
			else
				mAddr15 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR16:
			if(!val)
				mAddr16 &= ~(1 << bitNo);
			else
				mAddr16 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR17:
			if(!val)
				mAddr17 &= ~(1 << bitNo);
			else
				mAddr17 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR18:
			if(!val)
				mAddr18 &= ~(1 << bitNo);
			else
				mAddr18 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR19:
			if(!val)
				mAddr19 &= ~(1 << bitNo);
			else
				mAddr19 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR20:
			if(!val)
				mAddr20 &= ~(1 << bitNo);
			else
				mAddr20 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR21:
			if(!val)
				mAddr21 &= ~(1 << bitNo);
			else
				mAddr21 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR22:
			if(!val)
				mAddr22 &= ~(1 << bitNo);
			else
				mAddr22 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR23:
			if(!val)
				mAddr23 &= ~(1 << bitNo);
			else
				mAddr23 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR24:
			if(!val)
				mAddr24 &= ~(1 << bitNo);
			else
				mAddr24 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR25:
			if(!val)
				mAddr25 &= ~(1 << bitNo);
			else
				mAddr25 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR26:
			if(!val)
				mAddr26 &= ~(1 << bitNo);
			else
				mAddr26 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR27:
			if(!val)
				mAddr27 &= ~(1 << bitNo);
			else
				mAddr27 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR28:
			if(!val)
				mAddr28 &= ~(1 << bitNo);
			else
				mAddr28 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR29:
			if(!val)
				mAddr29 &= ~(1 << bitNo);
			else
				mAddr29 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR30:
			if(!val)
				mAddr30 &= ~(1 << bitNo);
			else
				mAddr30 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR31:
			if(!val)
				mAddr31 &= ~(1 << bitNo);
			else
				mAddr31 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR32:
			if(!val)
				mAddr32 &= ~(1 << bitNo);
			else
				mAddr32 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR33:
			if(!val)
				mAddr33 &= ~(1 << bitNo);
			else
				mAddr33 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR34:
			if(!val)
				mAddr34 &= ~(1 << bitNo);
			else
				mAddr34 |= 1 << bitNo;
			break;
		case EndexScanProtocols.BYTE_COMMAND_ADDR35:
			if(!val)
				mAddr35 &= ~(1 << bitNo);
			else
				mAddr35 |= 1 << bitNo;
			break;
		}
		return ;
	}
}
