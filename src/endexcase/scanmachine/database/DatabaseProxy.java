package endexcase.scanmachine.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import endexcase.scanmachine.AppData;
import endexcase.scanmachine.database.Table.MachineMemTable;
import endexcase.scanmachine.uart.EndexScanProtocols;
import endexcase.scanmachine.util.Consts;
import endexcase.scanmachine.util.Utils;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;

public class DatabaseProxy {
	private static DatabaseProxy mDatabaseProxy;
	private final DatabaseHelper mDatabase;
	private final SharedPreferences mSharedPreference;
	private static Context mContext;
	
	public static DatabaseProxy getInstance(Context context){
		if(mDatabaseProxy==null){
			synchronized (DatabaseProxy.class) {
				if(mDatabaseProxy==null){
					mDatabaseProxy = new DatabaseProxy(context.getApplicationContext());
				}
			}
		}
		
		mContext = context;
		
		return mDatabaseProxy;
	}
	
	private DatabaseProxy(Context applicationContext){
		mDatabase = new DatabaseHelper(applicationContext);
		mSharedPreference = applicationContext.getSharedPreferences(SharedPrefConstants.PREF_NAME, Context.MODE_MULTI_PROCESS);
	}
	
	
	public boolean initMachineMemData() {
		boolean result = mDatabase.initMachineMemData();
		mSharedPreference.edit().putBoolean(SharedPrefConstants.PREF_KEY_WORD_DB_INIT, true).commit();
		Utils.sync("initMachineMemData");
		return result;
	}
	
	public boolean clearMachineMemData() {
		boolean result = mDatabase.clearMachineMemData();
		int memId=0;
		int machineId=0;
		
		for(machineId=0; machineId<23; machineId++) {
			for(memId=0; memId<50; memId++) {
				Consts.clearMemDataFile(mContext.getFilesDir().toString(), machineId,  memId);
			}
		}
		
		return result;
	}
	
	public boolean getMachineMemData(int machineId) {
		AppData appdata = AppData.getInstance();
		
		Cursor cursor = mDatabase.getMachineMemData(machineId);
		if(cursor != null) {
			appdata.mAddr29 = cursor.getInt(cursor.getColumnIndex(MachineMemTable.COLUMN_MEM_ADDR29));
			appdata.mAddr30 = cursor.getInt(cursor.getColumnIndex(MachineMemTable.COLUMN_MEM_ADDR30));
			appdata.mAddr31 = cursor.getInt(cursor.getColumnIndex(MachineMemTable.COLUMN_MEM_ADDR31));
			appdata.mAddr32 = cursor.getInt(cursor.getColumnIndex(MachineMemTable.COLUMN_MEM_ADDR32));
			appdata.mAddr33 = cursor.getInt(cursor.getColumnIndex(MachineMemTable.COLUMN_MEM_ADDR33));
			appdata.mAddr34 = cursor.getInt(cursor.getColumnIndex(MachineMemTable.COLUMN_MEM_ADDR34));
			appdata.mAddr35 = cursor.getInt(cursor.getColumnIndex(MachineMemTable.COLUMN_MEM_ADDR35));
			return true;
		}
		return false;
	}
	
	public boolean updateMachineMemData(int machineId, int addr29, int addr30, int addr31, int addr32, int addr33, int addr34, int addr35) {
		return mDatabase.updateMachineMemData(machineId, addr29, addr30, addr31, addr32, addr33, addr34, addr35);
	}
	
	public boolean isDatabaseCreate() {
		return mSharedPreference.getBoolean(SharedPrefConstants.PREF_KEY_WORD_DB_INIT, false);
	}
	
	public boolean loadAppParameter() {
		AppData appdata = AppData.getInstance();
		
		int size = Consts.MACHINE_WORD_PARAMETER_SIZE;
		
		for( int i=0; i<size; i++) {
			appdata.mWordPara[i] = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_WORD_PARAM_ + i, 0);
		}
		
		appdata.mAddr15 = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR15, 0);
		appdata.mAddr16 = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR16, 0);
		appdata.mAddr17 = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR17, 0);
		appdata.mAddr18 = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR18, 0);
		appdata.mAddr19 = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR19, 0);
		appdata.mAddr20 = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR20, 0);
		appdata.mAddr21 = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR21, 0);
		appdata.mAddr22 = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR22, 0);
		appdata.mAddr23 = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR23, 0);
		appdata.mAddr24 = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR24, 0);
		appdata.mAddr25 = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR25, 0);
		appdata.mAddr26 = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR26, 0);
		appdata.mAddr27 = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR27, 0);
		appdata.mAddr28 = mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR28, 0);
		initFlagState();
		return true;
	}
	
	public boolean saveAppAllParameter() {
		AppData appdata = AppData.getInstance();
		
		int size = Consts.MACHINE_WORD_PARAMETER_SIZE;
		SharedPreferences.Editor mEdit = mSharedPreference.edit();
		
		for( int i=0; i<size; i++) {
			mEdit.putInt(SharedPrefConstants.PREF_KEY_WORD_PARAM_ + i, appdata.mWordPara[i]);
		}
		
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR15, appdata.mAddr15);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR16, appdata.mAddr16);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR17, appdata.mAddr17);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR18, appdata.mAddr18);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR19, appdata.mAddr19);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR20, appdata.mAddr20);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR21, appdata.mAddr21);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR22, appdata.mAddr22);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR23, appdata.mAddr23);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR24, appdata.mAddr24);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR25, appdata.mAddr25);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR26, appdata.mAddr26);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR27, appdata.mAddr27);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR28, appdata.mAddr28);
		mEdit.commit();
		
		Utils.sync("saveAppAllParameter");
        
		return true;
	}
	
	public boolean saveAppWordParameter(int addr) {
		AppData appdata = AppData.getInstance();
		SharedPreferences.Editor mEdit = mSharedPreference.edit();
		
		mEdit.putInt(SharedPrefConstants.PREF_KEY_WORD_PARAM_ + addr, appdata.mWordPara[addr]);
		mEdit.commit();
		
		Utils.sync("saveAppWordParameter");
		
		return true;
	}
	
	public boolean saveAppByteParameter() {
		AppData appdata = AppData.getInstance();
		SharedPreferences.Editor mEdit = mSharedPreference.edit();
		
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR15, appdata.mAddr15);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR16, appdata.mAddr16);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR17, appdata.mAddr17);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR18, appdata.mAddr18);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR19, appdata.mAddr19);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR20, appdata.mAddr20);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR21, appdata.mAddr21);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR22, appdata.mAddr22);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR23, appdata.mAddr23);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR24, appdata.mAddr24);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR25, appdata.mAddr25);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR26, appdata.mAddr26);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR27, appdata.mAddr27);
		mEdit.putInt(SharedPrefConstants.PREF_KEY_BYTE_ADDR28, appdata.mAddr28);
		mEdit.commit();
		
		Utils.sync("saveAppByteParameter");
		
		return true;
	}
	
	//Save APP data to Memory
	public boolean saveMemoryData(int machineId, int memId, String text) {
		AppData appdata = AppData.getInstance();
		
		String fileName = Consts.getMemDataFileName(mContext.getFilesDir().toString(), text, machineId,  memId);
		MemDataClass mClass = new MemDataClass();
		
		updateMachineMemData(machineId, 
				appdata.mAddr29, appdata.mAddr30, appdata.mAddr31, appdata.mAddr32, appdata.mAddr33,
				appdata.mAddr34, appdata.mAddr35);
		
		for(int i=0; i<Consts.MACHINE_WORD_PARAMETER_SIZE; i++) {
			mClass.mWordPara[i] = appdata.mWordPara[i];
		}
		mClass.mWordPara[EndexScanProtocols.WORD_COMMAND_RecallMemory_] = appdata.mWordPara[EndexScanProtocols.WORD_COMMAND_RecallMemory_] = memId + 1;
		
		mClass.mAddr15 = appdata.mAddr15;
		mClass.mAddr16 = appdata.mAddr16;
		mClass.mAddr17 = appdata.mAddr17;
		mClass.mAddr18 = appdata.mAddr18;
		mClass.mAddr19 = appdata.mAddr19;
		mClass.mAddr20 = appdata.mAddr20;
		
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(new File(fileName));
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(mClass);
			objectOutputStream.close();
			fileOutputStream.close();
			Utils.sync("saveMemoryData");
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		saveAppAllParameter();
		return true;
	}
	
	//Save APP from Memory data 
	public boolean readMemoryData(int machineId, int memId) {
		AppData appdata = AppData.getInstance();
		MemDataClass mClass = null;
		
		String fileName = Consts.findMemDataFileName(mContext.getFilesDir().toString(), machineId,  memId);
		
		try {
			FileInputStream fileInputStream = new FileInputStream(new File(mContext.getFilesDir().toString() + "/"+ fileName));
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			mClass = (MemDataClass) objectInputStream.readObject();
			objectInputStream.close();
			fileInputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}
		
		for(int i=0; i<Consts.MACHINE_WORD_PARAMETER_SIZE; i++) {
			appdata.mWordPara[i] = mClass.mWordPara[i];
		}
		
		appdata.mWordPara[EndexScanProtocols.WORD_COMMAND_RecallMemory_] = mClass.mWordPara[EndexScanProtocols.WORD_COMMAND_RecallMemory_] = memId+1;

		appdata.mAddr15 = mClass.mAddr15;
		appdata.mAddr16 = mClass.mAddr16;
		appdata.mAddr17 = mClass.mAddr17;
		appdata.mAddr18 = mClass.mAddr18;
		appdata.mAddr19 = mClass.mAddr19;
		appdata.mAddr20 = mClass.mAddr20;
		
		saveAppAllParameter();
		return true;
	}
	
	
	public void initFlagState(){
		AppData appdata = AppData.getInstance();
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR15, EndexScanProtocols.BYTE_COMMAND_ADDR15_CnyChkOnOff_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR16, EndexScanProtocols.BYTE_COMMAND_ADDR16_BotAutoMesEnable_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR16, EndexScanProtocols.BYTE_COMMAND_ADDR16_LabSenLAutoMesEnable_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR16, EndexScanProtocols.BYTE_COMMAND_ADDR16_LabSenRAutoMesEnable_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR16, EndexScanProtocols.BYTE_COMMAND_ADDR16_LabLenLAutoMesEnable_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR17, EndexScanProtocols.BYTE_COMMAND_ADDR17_LabLenRAutoMesEnable_, false);
		
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR18, EndexScanProtocols.BYTE_COMMAND_ADDR18_AutoMesAllEnable_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR18, EndexScanProtocols.BYTE_COMMAND_ADDR18_EditLabQtyEnable_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR18, EndexScanProtocols.BYTE_COMMAND_ADDR18_RunStopKey_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR18, EndexScanProtocols.BYTE_COMMAND_ADDR18_EditRsvQtyEnable_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR18, EndexScanProtocols.BYTE_COMMAND_ADDR18_LabelTestModeEnable_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR20, EndexScanProtocols.BYTE_COMMAND_ADDR20_CheckRollPaste_, false);
		appdata.setBitData(EndexScanProtocols.BYTE_COMMAND_ADDR20, EndexScanProtocols.BYTE_COMMAND_ADDR20_MainBoardIONoMatch_, false);
	}
	
	public int getWordParamData(int addr){
		return mSharedPreference.getInt(SharedPrefConstants.PREF_KEY_WORD_PARAM_ + addr, 0);
	}
}
