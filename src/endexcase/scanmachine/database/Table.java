package endexcase.scanmachine.database;

import android.provider.BaseColumns;

public class Table {
	public static final String TEXT_TYPE = " TEXT";
	public static final String INTEGER_TYPE = " INTEGER";
	public static final String REAL_TYPE = " REAL";
	public static final String COMMA_SEP = ",";
	
	public static class MachineMemTable implements BaseColumns{
		public static final String TABLE_NAME = "MachineMemTable";

		public static final String COLUMN_MACHINE_ID = "machineId";
		public static final String COLUMN_MEM_ADDR29 = "memAddr29";
		public static final String COLUMN_MEM_ADDR30 = "memAddr30";
		public static final String COLUMN_MEM_ADDR31 = "memAddr31";
		public static final String COLUMN_MEM_ADDR32 = "memAddr32";
		public static final String COLUMN_MEM_ADDR33 = "memAddr33";
		public static final String COLUMN_MEM_ADDR34 = "memAddr34";
		public static final String COLUMN_MEM_ADDR35 = "memAddr35";

		public static final String SQL_CREATE_TABLE =
				"CREATE TABLE " + TABLE_NAME + " (" +
						_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
						COLUMN_MACHINE_ID + INTEGER_TYPE + COMMA_SEP +
						COLUMN_MEM_ADDR29 + INTEGER_TYPE + COMMA_SEP +
						COLUMN_MEM_ADDR30 + INTEGER_TYPE + COMMA_SEP +
						COLUMN_MEM_ADDR31 + INTEGER_TYPE + COMMA_SEP +
						COLUMN_MEM_ADDR32 + INTEGER_TYPE + COMMA_SEP +
						COLUMN_MEM_ADDR33 + INTEGER_TYPE + COMMA_SEP +
						COLUMN_MEM_ADDR34 + INTEGER_TYPE + COMMA_SEP +
						COLUMN_MEM_ADDR35 + INTEGER_TYPE +
						// Any other options for the CREATE command
						" )";

		public static final String SQL_DELETE_TABLE_IF_EXIST =
				"DROP TABLE IF EXISTS " + TABLE_NAME;
		
		public static final String[] COLUMNS = new String[] {
			COLUMN_MACHINE_ID,
			COLUMN_MEM_ADDR29,
			COLUMN_MEM_ADDR30,
			COLUMN_MEM_ADDR31,
			COLUMN_MEM_ADDR32,
			COLUMN_MEM_ADDR33,
			COLUMN_MEM_ADDR34,
			COLUMN_MEM_ADDR35};
	}
}
